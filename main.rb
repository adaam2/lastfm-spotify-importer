require 'lastfm'
require 'date'

# last fm
# api key: 47c3ca8905212ccf8bfc091c2c7d0e46
# secret: 8bcf48807a2674894573cfbf0c4ea94e

def get_auth_url(token, api_key)
    "http://www.last.fm/api/auth/?api_key=#{api_key}&token=#{token}"
end

def get_formatted_track_name(obj)
    begin
        "#{obj["artist"]["content"]} - #{obj["name"]}\n"    
    rescue Exception => e
        return nil
    end
end

api_key = '47c3ca8905212ccf8bfc091c2c7d0e46'
api_secret = '8bcf48807a2674894573cfbf0c4ea94e'
lastfm = Lastfm.new(api_key, api_secret)
token = lastfm.auth.get_token

puts "Open #{get_auth_url(token, api_key)} and paste auth code back here"
puts "Press any key once done. \n"

go_ahead = gets

puts "Please enter your last.fm username:"

username = gets.chomp

puts "Your username is #{username}"

user_info = lastfm.user.get_info(:user => username)
#puts user_info.inspect

registered_date = DateTime.strptime(user_info["registered"]["unixtime"],'%s')
play_count = user_info["playcount"].to_i

puts "#{username} has #{play_count} plays. Whoa."

week_beginning = registered_date
tracks = []

while week_beginning <= Date.today
    week_ending = week_beginning.next_day(7)
    tracks_for_week = lastfm.user.get_weekly_track_chart(:user => username, :from => week_beginning.to_time.to_i, :to => week_ending.to_time.to_i)

    if tracks_for_week
        tracks_for_week.each do |t|
            track = get_formatted_track_name(t)
            open("output/#{username}.txt", "a") { |f| f << track } 
        end
    end
    

    week_beginning = week_ending
end


puts "Finished"


